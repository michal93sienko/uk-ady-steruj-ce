/*
 * wyswietlacz.c
 *
 * Created: 2015-11-25 17:59:43
 *  Author: Michał Sieńko
 */ 

#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

//tablica, którą bedziemy 'wyswietlać' odpowiednie diody by tworzyły dane cyfry
static uint8_t cyfra[] = {~0b00111111, ~0b00000110, ~0b01011011, ~0b01001111, ~0b01100110, ~0b01101101, ~0b01111101, ~0b00000111, ~0b01111111, ~0b01101111};
uint8_t i;
uint32_t TIME;

ISR(TIMER0_COMP_vect){ //obsługa przerwania, gdy wartość timera będzie równa OCR0
    TIME++; //liczy milisekundy
	
    if(TIME%4 == 0){ //'cykl' wyświetlania trwa 4 ms
        PORTB = ~8; //wyświetlacz pierwszy z prawej (najmłodszy)
        PORTA = cyfra[(TIME/1000)%10]; //co sekundę się zmienia
    }
    else if(TIME%4 ==1){
        PORTB =~4; //kolejny wyswietlacz-dalej analogicznie
        PORTA = cyfra[(TIME/10000)%10]; //zmienia się co 10 sekund
    }
    else if(TIME%4 ==2){
        PORTB = ~2;
        PORTA = cyfra[(TIME/100000)%10];
    }else{
        PORTB = ~1;
        PORTA = cyfra[(TIME/1000000)%10];
    }
}
/*
int main(void)
{
    uint8_t cyfra[] = {~0b00111111, ~0b00000110, ~0b01011011, ~0b01001111, ~0b01100110, ~0b01101101, ~0b01111101, ~0b00000111, ~0b01111111, ~0b01101111};
    DDRA = 0xFF;
    DDRB = 0xFF;
    PORTB = 0x00;
    uint8_t i=1;
    uint16_t TIME=0;
    uint8_t d1,d2,d3,d4;
    while(1)
    {
        TIME++;
        for(int j=0;j<50;j++){
            
            i=8;
            PORTB = ~i;
            PORTA = cyfra[TIME%10];
            _delay_ms(5);
            i=i>>1;
            PORTB =~i;
            PORTA = cyfra[(TIME/10)%10];
            _delay_ms(5);
            i=i>>1;
            PORTB = ~i;
            PORTA = cyfra[(TIME/100)%10];
            _delay_ms(5);
            i=i>>1;
            PORTB = ~i;
            PORTA = cyfra[(TIME/1000)%10];
            _delay_ms(5);
        }
        
    }
}*/

int main(void){
    //cyfra = 
    DDRA = 0xFF; //wyjście
    DDRB = 0xFF; //wyjście
    PORTB = 0x00; //a chyba w sumie niepotrzebne, ale oznacza tyle, że wszystkie wyświetlacze obsługiwane w danym momencie
    
    TIME=0;
    OCR0 = 250; //po 250 uderzeniach wywoluje przerwanie
    TIMSK |= 1 << OCIE0; //przerywa gdy OCR0 doliczy do swej wartości (250) tzn co 1ms jest przerwanie(bo prescaler = >64)
    TCCR0 |= 1 <<WGM01 | 0 << WGM00 | 0 << CS02 | 1 << CS01 | 1 << CS00; //ustawienie CTC i bierze z prescalera 64; 
	//16 000 000 /63 = 250 000       250 000 / 250 (co tyle uderzeń zgłasza przerwanie) = 1000  <=będzie liczył milisekundy
    sei();//włączenie przerwań
    while(1){}
}