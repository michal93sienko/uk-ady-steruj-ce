//MICHA� SIE�KO �RODA 18-19:30
/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#define F_CPU 16000000UL
#include <util/delay.h>
#include <stdbool.h>
#include <avr/io.h>
#define czekaj(czas) for(int i=0;i<(czas);i++) _delay_ms(1);  //stosowana do zmiennych op�nie�: delay_ms(i)

 void zadanie1(){
	 
	 if (PINA & 0b00000011) //gdy wci�ni�te dwa klawisze klawiatury
	 PORTB = 0xF0; //zaswiecaj� sie 4 'starsze' diody 
	 else
	 PORTB = 0x0F;  //w przeciwnym wypadku 4 "m�odsze" diody
 }
 void wezyk(){
	 bool kier=true;
	 PORTB = 0x01; //zaswiecona pierwsza dioda
	 while(1){
		 _delay_ms(200); //czekamy
		 if(kier){
			 PORTB = PORTB <<1; //dioda gasnie+zapala sie starsza
			 if(PORTB == 0b10000000)kier=!kier; //gdy dojdzie do konca zmieniamy kierunek
			 }else{
			 PORTB = PORTB >>1;
			 if(PORTB == 0b0000001)kier=!kier;
		 }
	 }
 }

 void sciemniacz(){
	 uint8_t i;
	 i=20;
	 bool kier=true; //kierunek.chodzi o ga�ni�cie lub zaswiecanie
	 while(1){
		 
		 for(uint8_t j=0;j<8;j++){ 
			 czekaj(i); //czas zgaszenia
			 PORTB = 0xff; //zaswiecamy wszystkie
			 czekaj(25-i); //czas �wiecenia wsyztskich
			 PORTB = 0x00; //gasimy
		 }
		 if(i==0)kier=!kier;
		 if(i==25)kier=!kier;
		 if(kier)i=i+5;else i=i-5;
	 }
 }
void cykl(uint8_t bit){ //parametrem jest najjasniejszy bit (od 0 do 7)
//gasn� trzy diody za najja�niejszym bitem a dwie przed nim sie rozja�niaj�
//jeden cykl trwa 20ms i jest powtarzany 5 razy
//najjasniejszy bit swieci 20/20, dwa obok 13/20, dwa dalsze 10/20 a najciemniejszy 5/20ms

	uint8_t i;
	if(bit == 4 || bit == 5 || bit == 3){//brak 'przeniesienia" 
		for(i=0;i<5;i++){
			PORTB = 1<<bit;
			_delay_ms(7);
			PORTB |= 1 << (bit+1)|1 << (bit-1);//swieca si� dodatkowe dwa bity kolo g�ownego
			_delay_ms(3);
			PORTB |= 1 << (bit+2)|1 << (bit-2); //swieca si� dodatkowe dwa bity 
			_delay_ms(5);
			PORTB |= 1<<(bit-3); //ostatni bit
			_delay_ms(5);
			PORTB = 0x00;//gasimy wszystko
		} 
	}else 
	if (bit == 6){
		for(i=0;i<5;i++){
			PORTB = 1<<6;
			_delay_ms(7);
			PORTB |= 1 << 7|1 << 5; 
			_delay_ms(3);
			PORTB |= 1 << 0 |1 << 4; 
			_delay_ms(5);
			PORTB |= 1<<3;
			_delay_ms(5);
			PORTB = 0x00;//gasimy wszystko
		}
	}else 
	if (bit == 7){
		for(i=0;i<5;i++){
			PORTB = 1<<7;
			_delay_ms(7);
			PORTB |= 1 << 0|1 << 6; 
			_delay_ms(3);
			PORTB |= 1 << 1 |1 << 5; 
			_delay_ms(5);
			PORTB |= 1<<4;
			_delay_ms(5);
			PORTB = 0x00;//gasimy wszystko
		}
	}else 
	if (bit == 0){
		for(i=0;i<5;i++){
			PORTB = 1<<0;
			_delay_ms(7);
			PORTB |= 1 << 1|1 << 7; //swieca si� dodatkowe dwa bity kolo g�ownego
			_delay_ms(3);
			PORTB |= 1 << 2 |1 << 6; //swieca si� dodatkowe cztery bity kolo g�ownego
			_delay_ms(5);
			PORTB |= 1<<5;
			_delay_ms(5);
			PORTB = 0x00;//gasimy wszystko
		}
	}
	else
	if (bit == 1){
		for(i=0;i<5;i++){
			PORTB = 1<<1;
			_delay_ms(7);
			PORTB |= 1 << 2|1 << 0; //swieca si� dodatkowe dwa bity kolo g�ownego
			_delay_ms(3);
			PORTB |= 1 << 3 |1 << 7; //swieca si� dodatkowe cztery bity kolo g�ownego
			_delay_ms(5);
			PORTB |= 1<<6;
			_delay_ms(5);
			PORTB = 0x00;//gasimy wszystko
		}
	} else 
	if (bit == 2){
		for(i=0;i<5;i++){
			PORTB = 1<<2;
			_delay_ms(7);
			PORTB |= 1 << 1|1 << 3; //swieca si� dodatkowe dwa bity kolo g�ownego
			_delay_ms(3);
			PORTB |= 1 << 0 |1 << 4; //swieca si� dodatkowe cztery bity kolo g�ownego
			_delay_ms(5);
			PORTB = 1<<7;
			_delay_ms(5);
			PORTB = 0x00;//gasimy wszystko
		}
	}
}

void waz_zaaw(){
//w�� chodzi 'dooko�a' tzn przechodzac za ostatnia diode pojawia si� na pierwszej
    uint8_t b;
    b = 3;
	
	while(1){
		cykl(b);	
		if(b == 7)
			b = 0;
		else
			b=b+1;
	}
}
int main (void)
{
	/* Insert system clock initialization code here (sysclk_init()). */

	board_init();//wygenerowane przez atmelStudio
	   DDRB |= 0xff; //ca�y port B na wyj�cie (diody)
	   DDRA |= 0x00; //ca�y port A na wej�cie
	   PORTA |= 0b00000011; //rezystory podci�gaj�ce na dwa najm�odsze bity<-przyciski klawiatury
	   
	   while(1)
	   {
		   waz_zaaw();
	   }
}
