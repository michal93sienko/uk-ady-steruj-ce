#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>



#define gora 1
#define lewo 1
#define enter 2
#define dol 4
#define prawo 4
#define exit2 8
#define nic 0  //zadne klawisz nie zostal wcisniety
#define czekaj(czas) for(int i=0;i<(czas);i++) _delay_ms(1);
//stosowana do zmiennych opóznień: delay_ms(i)
//tryby gry//
#define MODE1 0
#define MODE2 1
#define NOMODE -1
//nextParty//
#define MENU 10
#define WYNIK 15
#define WIN 1
#define DRAW 0
#define PRZEGRALES 30

//1 -> gora/lewy
//2 -> enter
//3 -> dol/prawy
//4 -> exit
//*****************//

void WriteNibble(unsigned char nibbleToWrite)
{

        PORTA |= (1<<PA4);


        PORTA = (PORTA & 0xF0)|(nibbleToWrite & 0x0F);

        PORTA &= ~(1<<PA4);


}


void WriteByte(unsigned char dataToWrite)
{
        WriteNibble(dataToWrite>>4); //starsza czworka
        WriteNibble(dataToWrite); // mlodsza czworka

}


void LCD_Command(unsigned char a)
{
        PORTA &= ~(1<<PA5); //RS =0;
        WriteByte(a);
};

void LCD_Text(char *a)
{
        PORTA |= (1<<PA5);
        while(*a)
        {
                WriteByte(*a);
                ++a;
                _delay_us(50);

        }

};
void LCD_GoToXY(unsigned char a , unsigned char b)
{
        LCD_Command (0x80| (b + (0x40 * a)));
        _delay_ms(2);
};

void LCD_Clear(void)
{
        LCD_Command(1);
        _delay_ms(2);
};

void LCD_Home(void)
{
        LCD_Command(2);
        _delay_ms(2);
};
void LCD_Cursor(void)
{
        LCD_Command(0b00011000);

        _delay_ms(2);
}

void LCD_Initalize()
{
        _delay_ms(50);
        for(int i=0; i<3; i++){
                WriteNibble(3);
                _delay_ms(5);
        }
        WriteNibble(2);
        _delay_ms(2);
        LCD_Command(0b00000010);
        _delay_ms(2);
        LCD_Command(0b00101000);
        _delay_ms(2);
        LCD_Command(0b00001000);
        _delay_ms(2);
        LCD_Command(0b00000001);
        _delay_ms(2);
        LCD_Command(0b00000110);
        _delay_ms(2);
        LCD_Command(0x0f);
        _delay_ms(2);
        LCD_Command(0x80);
        _delay_ms(2);



};

//************//
uint32_t TIME;
uint32_t TIME_SEC;
uint32_t TIME_10SEK;
uint32_t TIME_100SEK;
int FLAGA_GRY;
int USTAWIONY_CZAS_GRY;
int FLAGA_WYGRANIA;
int nextPart;
int LOS;
int PRESSED_KEY;

static uint8_t cyfra[] = {
        ~0b00111111, ~0b00000110, ~0b01011011,
        ~0b01001111, ~0b01100110, ~0b01101101,
        ~0b01111101, ~0b00000111, ~0b01111111, ~0b01101111
};
static char* words [] = {
"KAJAK", "WESTERPLATTE", "OOOOOOO", "ROWER",
"UNIWERSYTET", "NIE", "TAKTYKA", "KOMPUTER",
"KWIATEK", "WIATRAK", "GRAMATYKA", "WARSZAWA",
"WOREK", "ARYTMETYKA", "KOMPUTER","KLAWIATURA",
"FORTEPIAN","STUMILOWY","NEKROLOG","BOGURODZICA"
};

static uint8_t lenOfWords [] = {
        5,12,7,5,
        11,3,7,8,
        7,7,10,8,
        5,10,8,10,
        9,9,8,11
};

static char* menuOption []={
        "-> Bez limitu","   Bez limitu",
        "   Z limitem","-> Z limitem",
 "    Settings", " -> Settings"};



void initializeMain(){
          DDRD = 0xF0; //wejście klawiatury 0-3    kolumna wyswietlacza 4-7
          PORTD = 0xFF; //rezystor podciągajnący
          DDRC = 0xFF; //wyświetlacze siedmiosegmentowe (na wyjscie)
          DDRB = 0xFF; //diody
          DDRA = 0xFF; //LCD

          TIME=0;
          TIME_SEC=0;
          FLAGA_GRY=NOMODE;
          PRESSED_KEY=nic;

          TIME=0;
          OCR0 = 250;
      TIMSK |= 1 << OCIE0; //przerywa gdy OCR0 doliczy do swej
wartości(250) tzn co 1ms jest przerwanie
      TCCR0 |= (1 <<WGM01) | (0 << WGM00) | (0 << CS02) | (1 << CS01)
| (1<< CS00); //ustawienie CTC i bierze z prescalera 64;
      sei();

      LCD_Initalize();
}

//****************************************************//
//true jesli slowo sklada sie z jednej litery np.ooooo//
//****************************************************//

int isConsistOfOnlyOneTypeOfLetter(char *w,int len){
                int i;
                char znak = w[0]; //pierwszy znak
                for(i=1;i<len;i++)
                        if(znak != w[i]) return 1;
                return 0;
}
//*********************************************************************************//
//w <- slowo do porownania;posInWords <- inteks wzoru w tablicy; words
len <-dlugosc//
//*********************************************************************************//

int isDifferentThanPattern(char* w, char* posInWords, int len){
        int i;
        for(i=0;i<len;i++)
                        if(w[i] != posInWords[i]) return 1;
        return 0;
}
//*******************//
//odczyt z klawiatury//
//*******************//

int keyboard(){ //zwraca ktory klawisz zostal wcisniety
        //while(1){
        _delay_ms(100);
        if (~PIND  & 0b00000001){ return 1; }//gora
        if (~PIND & 0b00000010) return 2;//enter
        if (~PIND & 0b00000100) {return 4;}//dol
        if (~PIND & 0b00001000) return 8;//exit
                //}
                return nic;
                //trzy linijki dodane
                /*int result=PRESSED_KEY;
                while(result==nic){result=PRESSED_KEY;}
                        PRESSED_KEY=nic;
        return result;*/
                //return 0;
        //czy tutja będzie delay? tak z 400
        //PORTB = 0xF0; //zaswiecajš sie 4 'starsze' diody
        //else
        //PORTB = 0x0F;  //w przeciwnym wypadku 4 "młodsze" diody
}
// *********************************** //
// zamienia miejscami i z j w slowie w //
// *********************************** //
void swap ( char* w, int i,int j){
        if ( w[i] == w[j] ) return ; //nie trzeba nic zmieinac
        char tmp = w[i];
        w[i]=w[j];
        w[j]=tmp;
}

//**********************//
//miesza slowo mixedWord//
//**********************//

void mixWord(char* mixedWord, char* word, int len, int mode){
        int i;
        for(i=0;i<len;i++){mixedWord[i]=word[i];}
        if(mode == 0){
                                for( i=0;i<len;i++){

swap(mixedWord,i,(3*(i+len/2))%len) ;//coś miesza
                                }
                }else
                if (mode == 1){
                        for( i=0;i<len;i++){

swap(mixedWord,i,(3*i*(i+len/2))%len); //coś miesza ale inaczej
                                }
                }else
                if (mode == 2){
                        for( i=0;i<len;i++){

swap(mixedWord,i,(3*(i+3)*(len*i+i/2))%len); //coś miesza ale inaczej
                                }
                }
        //return mixedWord;
}

//**************//
//wyswietla Menu//
//**************//

void displayMenu(int a, int b){ //indexy w tabeli menuOption
        LCD_Clear();
                LCD_Text(menuOption[a]);
                LCD_GoToXY(15,0);
                LCD_Text(menuOption[b]);
        LCD_GoToXY(0,24);
}


//*******************//
//kontroluje Menu****//
//zwraca wybrany MODE//
//*******************//

int controlMenu(){ //zwraca ktora opcje menu user wybral
    uint8_t pressedKey = 0;
    uint8_t pos=0; //position - aktualnie wybierana opcja (nie
zatwierdzona jeszcze enterem)
    while(pressedKey != enter){ //nie enter
        pressedKey = keyboard();
        if(pressedKey == gora){//poszedl do gory
            pos = 0;//(pos==0) ? 0 : (pos-1);
          //  if ( pos == 0)
                                displayMenu(0,2);
            //else if ( pos == 1) displayMenu(3,4);

        } else if(pressedKey == dol){//poszedl na dol
                       pos=1;// if(pos == 0) pos = pos+1;
           // if ( pos == 1)
                                displayMenu(1,3);
            //else displayMenu(2,5);
        }else if (pressedKey==exit2){
            LCD_Clear();
            LCD_Text("NIE MA EXITOW!");
            _delay_ms(2000);
            displayMenu(0,2);
        }
    }
        //LCD_GoToXY(0,0);
        //char * str;
        //str = pos+ "0";
        //LCD_Text(str);
        //_delay_ms(1000);
    return pos; //0 - >MODE 1  1-> MODE 2  2->Settings
}

    //***********************************************//
        //*********Ustawianie Zegara dla MODE 2**********//
    //***********************************************//
void setTimer (int s,int d,int j,int pos){//setki dziesiatki jednosci
 aktualna pozycja kursora
         LCD_Clear();
         LCD_GoToXY(0,0);
                 char str[16] = {"Ustaw czas: "};
         str[12] = s + '0'; //ustawienie setek
         str[13] = d + '0';
         str[14] = j + '0';
         LCD_Text(str);//czy odpali?!!!1
                 LCD_GoToXY(0,pos+12);

}
//***************************************//
//wyświetlanie wyniku po zakonczonej grze//
//***************************************//

void setResult (int s,int d,int j){//setki dziesiatki jednosci
         LCD_Clear();
         LCD_GoToXY(0,0);
                LCD_Text("WYGRANA");
                LCD_GoToXY(15,0);
         char str[15] = {"Twoj czas: "};
         str[11] = s + '0'; //ustawienie setek
         str[12] = d + '0';
         str[13] = j + '0';
         LCD_Text(str);//czy odpali?!!!1
}

//**************************//
//ustawianie kontrola Zegara//
//**************************//

int controlTimer(){
        //LCD_GoToXY(0,11); //idize do pierwszego zera
        int pressedKey=nic;
        int liczba[3] = {0,0,0};
        int pos = 0; //0= setki 1=dziesiatki 2=jednostki
        setTimer(liczba[0],liczba[1],liczba[2],0);
        while(pressedKey!=enter){
                pressedKey=keyboard(); //czyta wciśnięcie klawisza
                if(pressedKey == gora){
                        liczba[pos]=(liczba[pos]+1)%10;
                }else if( pressedKey == dol){
                                        if(liczba[pos]==0)liczba[pos]=9;else
                        liczba[pos]=(liczba[pos]-1);
                }else if (pressedKey == exit2){//przechodzi w prawo (za mod 3)
                        pos = (pos+1)%3;
                       // LCD_GoToXY(0,(11+pos));
                }
        setTimer(liczba[0],liczba[1],liczba[2],pos);
        }
        return (liczba[0]*100 + liczba[1]*10+liczba[2]);
}


void clearArray(char* tab , int len){
        int i;

        for(i=0;i<len;i++){
                tab[i] = ' ';
        }
}

void copy(char* to ,char* from, int len){
        int i;
        for(i=0;i<len;i++){
                to[i]=from[i];
        }
}


//uint32_t STARTGAME;

//***************************************************************//
//Losuje slowo; miesza je; sprawdza czy jest poprawnie wymieszane//
//***************************************************************//

void initializeGame(char* mixedWord){ //miesza slowo w argumencie

         do{ //petla gdyby zadne z trybow mieszania nie dawal innego niz wzór słowa
         do{
                LOS = TIME%20;   
        }while(!isConsistOfOnlyOneTypeOfLetter(words[LOS],lenOfWords[LOS]));

                 clearArray(mixedWord,20);
                 copy(mixedWord, words[LOS], lenOfWords[LOS]);

                 //mieszanie//
                 //int mode = LOS%3;
                 int mode = TIME%3;
                 mixWord(mixedWord,words[LOS], lenOfWords[LOS],mode);
                 if( isDifferentThanPattern(mixedWord,words[LOS], lenOfWords[LOS] )
)return; //mixWord;
                mode = (mode + 1)%3;
                mixWord(mixedWord,words[LOS], lenOfWords[LOS],mode);
                 if( isDifferentThanPattern(mixedWord,words[LOS], lenOfWords[LOS] )
)return;// mixWord;
                mode = (mode + 1)%3;
                mixWord(mixedWord,words[LOS], lenOfWords[LOS],mode);
                 if( isDifferentThanPattern(mixedWord,words[LOS], lenOfWords[LOS] ))
return;// mixWord;

        }while(1<2);
}

//***********************************//
//wyswietla aktualny stan podczas gry//
//***********************************//

void graj(char* a, int pos){ //wyswietla odpowiednie slowo i cursor w
odpowiedinm miejscu
        LCD_Clear();
        LCD_GoToXY(0,0);
        LCD_Text(a); //czy to przejdzie?? moze bez gwiazdki?
        LCD_GoToXY(0,pos);
}

uint32_t random(){
        return TIME;
}

//**************************************************//
//kontroluje gre dopoki slowo jest rozne od patterna//
//***gra dopoki FLAGA_WYGRANIA != DRAW**************//
//**************************************************//

void controlGame(char* mixedWord, char* pattern, int len){
        graj(mixedWord,0);
        int pressedKey=nic;
        int pos=0;
        while(isDifferentThanPattern(mixedWord,pattern,len) == 1){
                pressedKey=keyboard();

                if(FLAGA_WYGRANIA == DRAW) return;//jesli czas minal to wywala
                if(pressedKey == lewo){
                         pos = (pos==0) ? 0 : (pos-1);
                }else if( pressedKey == prawo){
                         pos = (pos==len-1) ? pos : (pos+1);
                }else if (pressedKey == enter){//swapuje ta z kolejna z prawej
                        if (pos != (len-1)){
                                swap(mixedWord,pos,pos+1);
                                                                pos = pos+1;
                        }
                }
                graj(mixedWord,pos);
        }
        //return 0;
}

//************//
//obsluga diod//
//************//

void sciemniacz(){
         uint8_t t;
         t=5;
         int kier=1; //kierunek.chodzi o gasnięcie lub zaswiecanie
         while(1){

                 for(uint8_t j=0;j<7;j++){
                         czekaj(11-t); //czas zgaszenia
                         PORTB = 0xff; //zaswiecamy wszystkie
                         czekaj(t); //czas swiecenia wsyztskich
                         PORTB = 0x00; //gasimy
                        // if(PRESSED_KEY!=nic)return;
                          if (~PIND  & 0b00000001) return;
                        if (~PIND & 0b00000010) return ;
                        if (~PIND & 0b00000100) return ;
                        if (~PIND & 0b00001000) return ;
                 }
                 if(t<1)kier=-1*kier;
                 if(t>10)kier=-1*kier;
                 if(kier>0)t=t+1;else t=t-1;
         }
 }



//uint8_t i,kolumna,wiersz;

//**************************************************//
//wyswietla na siedmiosegmentowych odpowiednia cyfre//
//**************************************************//

void displayOnSevenSegment(int czasSek){
        int czasSetki = (czasSek%1000)/100;
        int czasDziesiatki = (czasSek%100)/10;
        czasSek = czasSek%10;
        if(TIME%4 == 0){

        PORTD =  (PORTD & 0x0F) | 0b11100000;
        PORTC = cyfra[czasSek];
    }
    else if(TIME%4 ==1){
        PORTD =  (PORTD & 0x0F) | 0b11010000;
        PORTC = cyfra[czasDziesiatki];
    }
    else if(TIME%4 ==2){
        PORTD =  (PORTD & 0x0F) | 0b10110000;
        PORTC = cyfra[czasSetki];
        }else{
        PORTD =  (PORTD & 0x0F) | 0b01110000;
        PORTC = cyfra[0];
    }

}

//********************//
//PRZERWANIE**********//
//odlicza LIMIT*******//
//obsluguje klawiature//
//liczy czas**********//
//********************//


ISR(TIMER0_COMP_vect){
// PORTD obsluguje kolumny
//PORTC obsluguje siedmiosegment
    TIME++; //liczy milisekundy

    TIME_SEC = TIME/1000; //sekundy
    TIME_10SEK = (TIME_SEC%100)/10;
    TIME_100SEK = (TIME_SEC%1000)/100;

        //********************//
        //OSLUGA KLAWIATURY   //
        //********************//
        /*if(TIME%130 == 0){//co 100ms sprawdza kalwiature
        PRESSED_KEY=nic;
         if (~PIND  & 0b00000001){ PRESSED_KEY=gora; }
       else if (~PIND & 0b00000010) PRESSED_KEY=enter;
       else if (~PIND & 0b00000100) {PRESSED_KEY=dol;}
       else if (~PIND & 0b00001000) PRESSED_KEY = exit2;
        }
        */
    if(FLAGA_GRY == MODE1) displayOnSevenSegment(TIME_SEC);
    else if(FLAGA_GRY==MODE2 ){
                displayOnSevenSegment(USTAWIONY_CZAS_GRY);
                if(TIME % 1000 == 0) USTAWIONY_CZAS_GRY--;//co sekunde stad %1000
                if( USTAWIONY_CZAS_GRY <= 0){//czyli pzrregrales
                        FLAGA_WYGRANIA = DRAW;
                        nextPart = PRZEGRALES; //chyba WYNIK moze byc


                }

        }//NOMODE
        else displayOnSevenSegment(0);
}

//RS -> PA5
/*void oAutorach(){
        LCD_Clear();
        LCD_GoToXY(0,0);
        LCD_Text("PAWEL");

        LCD_GoToXY(15,0);
        LCD_Text("MICHAL");
        LCD_GoToXY(0,24);

}*/

//****************************************************//
//przyjmuje jako argument czesc gry do ktorej ma wejsc//
//wywoluje odpowiednie kontrolery*********************//
//****************************************************//

void gameController(int gamePart){
        if(gamePart == MENU){
                displayMenu(0,2);
                FLAGA_GRY=NOMODE;
                FLAGA_GRY = controlMenu();
                nextPart = FLAGA_GRY;
                FLAGA_WYGRANIA = WIN;
                return;
        } else if(gamePart == MODE1){
                //int wyzwanie;
                char mixedWord[20];
                initializeGame(mixedWord);
                //FLAGA_GRY=MODE1;
                TIME = 0; //liczy od nowa
                FLAGA_GRY = MODE1;

                FLAGA_WYGRANIA = WIN;
                graj(mixedWord,0);
                controlGame(mixedWord,words[LOS],lenOfWords[LOS]);

                nextPart=WYNIK;
                FLAGA_GRY = WIN;
                FLAGA_GRY=NOMODE;
                return;
        } else if(gamePart == MODE2){
                FLAGA_GRY=NOMODE;
                char mixedWord[20];
                initializeGame(mixedWord);
                USTAWIONY_CZAS_GRY = controlTimer();//wczytuje czas
                FLAGA_GRY=MODE2;
                TIME=0;
                FLAGA_WYGRANIA = WIN;
                graj(mixedWord,0);
                controlGame(mixedWord,words[LOS],lenOfWords[LOS]);
                FLAGA_GRY=NOMODE;
//serio ta linijka potrzebna?
                        if(gamePart != PRZEGRALES)
                        nextPart=WYNIK; //bo PRZEGRALES wstawia Przerwanie gdy się przegra : )
                return;

        }else if(gamePart == WYNIK){
                FLAGA_GRY=NOMODE;
                if(FLAGA_WYGRANIA == DRAW){

                        LCD_Clear();
                        LCD_Text("PRZEGRALES");
                        LCD_GoToXY(15,0);
                        LCD_Text("WCISNIJ DOWOLNY KLAWISZ");
                        LCD_GoToXY(0,24);
                        FLAGA_GRY=NOMODE;
                        //_delay_ms(5000);
                        while(1){
                                if(keyboard()!=nic)break;
                                /*if (~PIND  & 0b00000001) break;
                                if (~PIND & 0b00000010) break ;
                                if (~PIND & 0b00000100) break ;
                                if (~PIND & 0b00001000) break ;*/
                        }
                }
                else{//wygrana
                LCD_GoToXY(0,0);
                LCD_Text("WYGRANA");
                //chyba nie bedzie widac w ogole
                int j=TIME_SEC%10;
                int d=TIME_10SEK;
                int s=TIME_100SEK;
                setResult(s,d,j);
                _delay_ms(300);
                sciemniacz();//czeka na wcisniecie klawisza
                }
                nextPart = MENU;
                return;
        }
        else if(gamePart == PRZEGRALES){
                while(1){
                        if(keyboard() != nic) break; //po wcisnieciu klawisza wywala
                }
                nextPart = MENU;
                return;
        }
        else{
                nextPart=MENU;//tak na wszelki wypadek
                return;
        }
}

int main(void)
{
          initializeMain();
                nextPart=MENU;
                while(1){
                        gameController(nextPart);
                }