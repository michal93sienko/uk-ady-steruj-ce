/*
 * GccApplication1.c
 *
 * Created: 2015-12-02 17:54:12
 *  Author: Administrator
 */ 
#define F_CPU 16000000UL
#include <avr/io.h>
#include <avr/interrupt.h>

static uint8_t cyfra[] = {~0b00111111, ~0b00000110, ~0b01011011, ~0b01001111, ~0b01100110, ~0b01101101, ~0b01111101, ~0b00000111, ~0b01111111, ~0b01101111};
uint8_t i,kolumna,wiersz;
uint32_t TIME;
static uint8_t konwerter[] = {0,0,1,0,2,0,0,0,3}; //konwertować będzie "wciśniety bit" na nr wiersza np 2=>1, 8=>3

ISR(TIMER0_COMP_vect){
    TIME++; //liczy milisekundy
    
    if(TIME%50 == 0){//wykonuje sie co 50ms sprawdzenie wcisniecia klawisza
        wiersz=PINA & 0b00001111; //zczytuje to co jest na porcie A na czterech najmłodszych bitach
		if(wiersz != 0b00001111) { //któryś klawisz wciśnięty
			wiersz = ~wiersz & 0x0F; //gdy wciśnięty klawisz na jego linii pojawia się 0
			wiersz = konwerter[wiersz];
			
			DDRA = ~DDRA; //zmiana kierunków portuA < -do niego podłączona jest klawiatura
			PORTA = ~PORTA;
			
			for(int j=0;j<10;j++); //odczekujemy
			kolumna = PINA & 0b11110000; //odczyt kolumny;
			kolumna = ~kolumna & 0xF0;
			kolumna = kolumna >>4;
			kolumna = konwerter[kolumna];
			wiersz = wiersz*4 + kolumna + 1; //+1 bo numerujemy od 1 a nie od 0. tutaj wiersz staje się naszym wynikiem
			DDRA = ~DDRA; //zmiana kierunków portuA 
			PORTA = ~PORTA;
		}
    }
    if(TIME%4 == 0){
        
        PORTD = ~8;
        PORTB = cyfra[wiersz%10];
    }
    else if(TIME%4 ==1){
        PORTD =~4;
        PORTB = cyfra[wiersz/10];
    }
    else if(TIME%4 ==2){
        PORTD = ~2;
        PORTB = cyfra[0];
        }else{
        PORTD = ~1;
        PORTB = cyfra[0];
    }
}

int main(void)
{
    DDRA = 0xF0; //wejscie cztery najmlodsze bity
    PORTA = 0x0F; //na bitach wejsciowych rezystory podciagajace  a na wyjściu zera
    DDRB = 0xFF;//cyfra
    DDRD = 0xFF;//numer wyswietlacza 7dmiosegmentowego
     TIME=0;
     OCR0 = 250;
     TIMSK |= 1 << OCIE0; //przerywa gdy OCR0 doliczy do swej wartości (250) tzn co 1ms jest przerwanie
     TCCR0 |= 1 <<WGM01 | 0 << WGM00 | 0 << CS02 | 1 << CS01 | 1 << CS00; //ustawienie CTC i bierze z prescalera 64;
     sei();
    while(1)
    {
        //TODO:: Please write your application code 
    }
}