/*
 * Zadanie2.c
 *
 * Created: 2015-11-19 19:00:52
 * Author : Michał
 */ 

#define  F_CPU 16000000UL


#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
uint8_t x;

ISR (INT0_vect){
    x++;
    PORTA &= 0x01; //kasuje bity poza tym jednym mrugajacym
	PORTA |= (x << 1); //wpisuje x przesunietego o 1
}
void setup(){
    DDRD = 0;  //port D na wejście
    PORTD |= (1<< PD2); //żeby nie wisiał w powietrzu
    DDRA = 0xFF; //port A na wyjście
    MCUCR = 1 << ISC01 | 0 << ISC00 ; //opadające zbocze INT0
    GICR = 1 <<INT0 ; //włącza zewn przerwanie
    sei(); //włączenie przerwań
}
void blink(){
    _delay_ms(200);
    PORTA ^= 0x01; //miganie najmłodsza diodą
}
/*
int main(void)
{
    x=0;
    setup();
    while(1)
    {
         blink();        
    }
}
*/
int main(void){
    DDRA = 0x03; //dwa bity portu A na wejście: klawiatura
    PORTA = 0b00000011; // rezystory podciągające
    OCR0 = 00; //ustawienie licznika na 0;
    DDRB = 0xff; //port B na wyjscie-tutaj interesuje nas pin PB3 => OC0 – wyjcie porównania licznika TC0 które idzie do diody
    TCCR0 = 1 << WGM01 | 1 <<WGM00 | 1 << COM01 | 0 << COM00 | 1 << CS02 | 0<< CS01 | 0<< CS00;
	//ustawianie TCCR0: Fast PWM  , czyszczenie OC0 gdy doliczy do górnej granicy, wybrany clock clk/256
    while(1){
            if( !(PINA & 0x01)) //nacisniecie przycisku
                OCR0++; //licznik bedzie doliczał do większej liczby=>dłużej świeci dioda
            if( !(PINA & 0x02)) // nacisniecie drugiego przycisku
                OCR0--; //analogicznie na odwrót
            _delay_ms(10);
    }
    
}

