#include "HD44780.h"
#define F_CPU 16000000UL
#include <util/delay.h>


#define E 1<<PA4
#define RS 1<<PA5

void WriteNibble(unsigned char nibbleToWrite)
{
        PORTA |= E;
        PORTA = (PORTA & 0xF0) | (nibbleToWrite & 0x0F);
        PORTA &= ~E;
}


void WriteByte(unsigned char dataToWrite)
{
        WriteNibble(dataToWrite >> 4);
        WriteNibble(dataToWrite);
}


void LCD_Command(unsigned char command)
{
        PORTA &= ~RS;
        WriteByte(command);
        _delay_ms(1);
};

void LCD_Text(char * text)
{

        PORTA |= RS;
        int i = 0;
        while (text[i] != '\0'){
                WriteByte(text[i++]);
                _delay_us(50);
        }

};

void LCD_GoToXY(unsigned char x, unsigned char y)
{
        char adr = y*0x40 +x;
        LCD_Command(adr | 0x80) ;
        _delay_ms(1);


};



void LCD_Clear(void)
{
        LCD_Command(1);
        _delay_ms(1);
};

void LCD_Home(void)
{
        LCD_Command(2);
        _delay_ms(1);
};

void LCD_Initalize(void)
{
        _delay_ms(50);
        for(int i=0; i<3; i++){
                WriteNibble(0x03);
                _delay_ms(5);
        }
        WriteNibble(0x02);
        _delay_ms(2);

        LCD_Command(0x28);
        _delay_ms(2);

        LCD_Command(0x08);
        _delay_ms(2);

        LCD_Command(0x01);
        _delay_ms(2);

        LCD_Command(0x06);
        _delay_ms(2);

        LCD_Command(0b00001100); //brak kursora
        _delay_ms(2);

};
void LCD_ShiftText(char * text, uint8_t len){


        for(uint8_t i=0;i<16-len;i++){
                LCD_Clear();
                LCD_GoToXY(i,0);
                LCD_Text(text);
                _delay_ms(400);
        }
        //char t2[len];
        for(uint8_t i=0;i<len-1;i++){
                LCD_Clear();
                LCD_GoToXY(16-len+1+i,0);
                LCD_Text(text);
                LCD_GoToXY(0,0);
                LCD_Text(text+len-1-i);
                _delay_ms(400);

        }
}

void LCD_shiftDisplay(){
        LCD_Command(0b00011100) ; //tekst sie bedzie przesuwal w prawo
        _delay_ms(300);
}

int main(void){
        DDRA = 0xFF;
        LCD_Initalize();
        LCD_Clear();
        //LCD_Text("dziala");
        LCD_Home();
        //LCD_GoToXY(15,0);
        //LCD_Text("dziala");
        //LCD_Text("cojest?");
        //LCD_ShiftText("JAKIS TEXT",10);
        LCD_Text("   WESOLYCH SWIAT BOZEGO NARODZENIA   ");
        LCD_GoToXY(0,1);
        LCD_Text("     I SZCZESLIWEGO NOWEGO ROKU   ");
        while(1){
                LCD_shiftDisplay();

        }

}